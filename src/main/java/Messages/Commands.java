package Messages;

import java.util.EnumSet;

/**
 * Created by xxinf on 31-Oct-16.
 */
public enum Commands {
    //access control commands
    AUTH("auth", "Register new User in server.\nNot implemented command\n"),
    ACCT("acct", "Not implemented command\n"),
    CDUP("cdup", "Change current directory to parent directory.\nFormat: CDUP <CRLF>\n"),
    CWD("cwd", "Change current directory to input path.\nFormat: CWD  <SP> <pathname> <CRLF>\n"),
    PASS("pass", "Sends user password for login.\nFormat: PASS <SP> <password> <CRLF>\n"),
    SMNT("smnt", "Not implemented command\n"),
    USER("user", "Send user login.\nFormat: USER <SP> <username> <CRLF>\n"),
    REIN("rein", "Reinitialize user.\nFormat: REIN <CRLF>\n"),
    QUIT("quit", "Close connection end exit.\nFormat: QUIT <CRLF>\n"),
    FEAT("feat", "System status.\nFormat: FEAT <CRLF>\n"),
    //file service commands
    SIZE("size", "Return the size of a file.\nFormat: SIZE  <SP> <filename> <CRLF>\n"),
    ABOR("abor", "Abort all file sending.\nFormat: ABOR <CRLF>\n"),
    ALLO("allo", "Allocating memory, obsolete command.\nNot implemented\n"),
    APPE("appe", "Append to file.\nFormat: APPE <SP> <pathname> <CRLF>\n"),
    DELE("dele", "Delete file by given path.\nFormat: DELE <SP> <pathname> <CRLF>\n"),
    LIST("list", "Return list of files and directories with info. by given path.\n" + "Format: LIST [<SP> <pathname>] <CRLF>\n"),
    MKD("mkd", "Create directory by given path.\nFormat: MKD  <SP> <pathname> <CRLF>\n"),
    XMKD("xmkd","Create directory by given path.\nFormat: MKD  <SP> <pathname> <CRLF>\n"),
    NLST("nlst", "Return list of files and directories by given path.\nFormat: NLST [<SP> <pathname>] <CRLF>\n"),
    PWD("pwd", "Return current directory.\nFormat: PWD  <CRLF>\n"),
    XPWD("xpwd","Return current directory.\nFormat: PWD  <CRLF>\n"),
    REST("rest","Restart sending file.\nNor implemented.\n"),
    RETR("retr", "Get file by given path.\nFormat: RETR <SP> <pathname> <CRLF>\n"),
    RMD("rmd", "Remove directory by given path.\nFormat: RMD  <SP> <pathname> <CRLF>\n"),
    XRMD("xrmd", "Remove directory by given path.\nFormat: RMD  <SP> <pathname> <CRLF>\n"),
    RNFR("rnfr", "Firs part of rename or move command.\nFormat: RNFR <SP> <pathname> <CRLF>\n"),
    RNTO("rnto", "Second part of rename or move command.\nFormat: RNTO <SP> <pathname> <CRLF>\n"),
    STOR("stor", "Save given file to server.\nFormat: STOR <SP> <pathname> <CRLF>\n"),
    STOU("stou", "Save given file with unique name.\nFormat: STOU <SP> <pathname> <CRLF>"),
    SITE("site", "Not implemented command.\n"),
    SYST("syst", "Return server system type.\nFormat: SYST <CRLF>\n"),
    STAT("stat", "Not implemented command.\n"),
    HELP("help", "Commands:\n" +
            "AUTH* ACCT* CDUP CWD PASS SMNT USER REIN QUIT FEAT SIZE ABOR ALLO* APPE DELE\n" +
            "LIST MKD XMKD NLST PWD XPWD REST* RETR RMD XRMD RNFR RNTO STOR STOU SITE*\n" +
            "SYST STAT* HELP NOOP MODE PASV PORT STRU* TYPE OPTS MDTM\n\n" +
            "* - Not implemented commands!\n"),
    NOOP("noop", "Server pulse command.\nFormat: NOOP <CRLF>\n"),
    MDTM("mdtm", "Return last modification date.\nFormat: <SP> <pathname> <CRLF>\n"),
    //transfer parameters commands
    MODE("mode", "Obsolete command of file transfer mode.\nStream format by default\nFormat: MODE <CELF>\n"),
    PASV("pasv", "Passive mode of data connection.\nFormat: PASV <CRLF>\n"),
    PORT("port", "Active mode of data connection.\nFormat: PORT <SP> <host-port> <CRLF>\n"),
    STRU("stru", "Mount root directory of server.\nNot implemented command.\n"),
    TYPE("type", "Sending file type.\nFormat: TYPE <SP> <type-code> <CRLF>\n"),
    OPTS("opts", "Accepting coding.\nFormat: OPTS <SP> <type-code> <CRLF>");

    private String name;
    private String help;

    private Commands(String name, String help) {
        this.name = name;
        this.help = help;
    }

    public String getHelp() {
        return help;
    }

}
