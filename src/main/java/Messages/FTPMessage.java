package Messages;

/**
 * Created by xxinf on 03-Nov-16.
 */
public class FTPMessage  {
    private String message;

    public FTPMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
