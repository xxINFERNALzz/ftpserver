package Messages;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by xxinf on 02-Nov-16.
 */
public enum RepliesCodes {
    /**
     * In this case, the text is exact and not left to the
     * particular implementation; it must read:
     * MARK yyyy = mmmm
     * Where yyyy is User-process data stream marker, and mmmm
     * server's equivalent marker (note the spaces between markers
     * and "=").
     */
    C_110(110, "Restart marker reply."),
    C_120(120, "Service ready in %s minutes."),
    C_125(125, "Data connection already open; transfer starting."),
    C_150(150, "File status okay; about to open data connection."),
    C_200(200, "Command okay."),
    C_202(202, "Command not implemented, superfluous at this site."),
    C_211(211, "System status, or system help reply."),
    C_212(212, "Directory status."),
    C_213(213, "%S"),
    /**
     * On how to use the server or the meaning of a particular
     * non-standard command.  This reply is useful only to the
     * human user.
     */
    C_214(214, "Help message:\n%s"),
    /**
     * Where NAME is an official system name from the list in the
     * Assigned Numbers document.
     */
    C_215(215, "%s system type."),
    C_220(220, "Service ready for new user."),
    /**
     * Logged out if appropriate.
     */
    C_221(221, "Service close control connection. Bye!"),
    C_225(225, "Data connection open; no transfer in progress."),
    /**
     * Requested file action successful (for example, file
     * transfer or file abort).
     */
    C_226(226, "Closing data connection."),
    C_227(227, "Entering Passive Mode (%d,%d,%d,%d,%d,%d)."),
    C_230(230, "%s logged in, proceed."),
    C_250(250, "Requested file action okay, completed."),
    C_257(257, "%s is current directory."),
    C_331(331, "%s okay, need password."),
    C_332(332, "Need account for login."),
    C_350(350, "Requested file action pending further information."),
    /**
     * This may be a reply to any command if the service knows it
     * must shut down.
     */
    C_421(421, "Service not available, closing control connection."),
    C_425(425, "Use PORT of PASV first."),
    C_426(426, "Connection closed; transfer aborted."),
    /**
     * File unavailable (e.g., file busy).
     */
    C_450(450, "Requested file action not taken."),
    C_451(451, "Requested action aborted: local error in processing."),
    /**
     * Insufficient storage space in system.
     */
    C_452(452, "Requested action not taken."),
    /**
     * This may include errors such as command line too long.
     */
    C_500(500, "Syntax error, command unrecognized."),
    C_501(501, "Syntax error in parameters or arguments."),
    C_502(502, "Command not implemented."),
    C_503(503, "Bad sequence of commands."),
    C_504(504, "Command not implemented for that parameter."),
    C_530(530, "Not logged in."),
    C_532(532, "Need account for storing files."),
    /**
     * File unavailable (e.g., file not found, no access).
     */
    C_550(550, "Requested action not taken."),
    C_551(551, "Requested action aborted: page type unknown."),
    /**
     * Exceeded storage allocation (for current directory or
     * dataset).
     */
    C_552(552, "Requested file action aborted."),
    /**
     * File name not allowed.
     */
    C_553(553, "Requested action not taken.");


    private String description;
    private int code;

    private RepliesCodes(int code, String name) {
        this.description = name;
        this.code = code;
    }

    public String getCodeReplies(Object... params) {
        if (params.length == 0)
            return code + " " + description + "\r\n";
        else
            return String.format(code + " " + description + "\r\n", params);
    }

    public String getCodeMessage(String description) {
        return code + " " + description + "\r\n";
    }

}
