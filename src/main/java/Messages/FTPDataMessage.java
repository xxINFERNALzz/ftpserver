package Messages;

/**
 * Created by xxinf on 05-Dec-16.
 */
public class FTPDataMessage {

    private final Commands command;
    private final String data;

    public Commands getCommand() {
        return command;
    }

    public String getData() {
        return data;
    }

    public FTPDataMessage(Commands command, String data) {
        this.command = command;
        this.data = data;
    }
}
