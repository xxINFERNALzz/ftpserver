package sample;

import Util.SettingsParser;
import akka.actor.ActorSystem;

/**
 * Created by xxinf on 31-Oct-16.
 */
public class Main {
    public static void main(String[] args) {
        if (SettingsParser.getInstance() != null && SettingsParser.getInstance().createSettings()) {
            ActorSystem actorSystem = ActorSystem.create("Main");
            actorSystem.actorOf(FTPServer.props(), "FTPServer");
        } else {
            System.err.println("Cannot parse settings file or cannot create it!");
        }
    }
}
