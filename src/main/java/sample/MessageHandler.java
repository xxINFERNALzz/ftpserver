package sample;

import Messages.Commands;
import Messages.FTPDataMessage;
import Messages.RepliesCodes;
import Messages.FTPMessage;
import Util.SettingsParser;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.io.Tcp;
import akka.io.TcpMessage;
import akka.japi.Creator;
import akka.util.ByteString;
import com.sun.istack.internal.NotNull;
import scala.collection.JavaConversions;
import scala.concurrent.duration.Duration;
import scala.concurrent.forkjoin.ThreadLocalRandom;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

/**
 * Created by xxinf on 01-Nov-16.
 */
public class MessageHandler extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    private final static EnumSet<Commands> COMMAND_SET = EnumSet.allOf(Commands.class);
    private final static String ANONYMOUS = "anonymous";
    private final static String NEW_FILE = "New File";
    public final static int PORT_FACTOR = 256;
    private final static int MIN_PORT_NUM = 1024;
    private final static int MAX_PORT_NUM = 65535;

    private final InetAddress remoteInetAddress;
    private final ActorRef client;
    private final InetAddress serverInetAddress;

    //for Active connection
    private InetAddress clientFileServerAddr;
    private int clientFileServerPort;

    //for passive connection
    private int fileServerPort = 3256;

    private ActorRef fileServer;
    private boolean isActive = true;

    private String userName = ANONYMOUS;
    private String pass = "";

    private final String rootDirectory;
    private String currentDirectory;

    private File rnfrFile = null;

    public static Props props(final InetAddress remoteInetAddress,
                              final InetAddress serverInetAddress,
                              @NotNull final ActorRef client) {
        return Props.create(new Creator<MessageHandler>() {
            public MessageHandler create() throws Exception {
                return new MessageHandler(remoteInetAddress, serverInetAddress, client);
            }
        });
    }

    private MessageHandler(InetAddress remoteInetAddress,
                           InetAddress serverInetAddr,
                           @NotNull ActorRef client) {
        this.remoteInetAddress = remoteInetAddress;
        this.client = client;
        this.serverInetAddress = serverInetAddr;
        this.rootDirectory = this.currentDirectory = SettingsParser.getInstance().getWorkingDirectory();
    }

    @Override
    public void preStart() throws Exception {
        log.info(String.format("%s connected.", remoteInetAddress));
        client.tell(
                TcpMessage.write(
                        ByteString.fromString(
                                RepliesCodes.C_220.getCodeReplies())), self());
    }

    public void onReceive(Object message) throws Exception {
        if (message instanceof Tcp.Received) {
            final String msg = ((Tcp.Received) message).data().utf8String();
            log.info("Received message: " + msg);
            String[] msgParts = msg.trim().split(" ");
            Commands command = null;
            try {
                command = Commands.valueOf(msgParts[0].toUpperCase());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            //if command is valid
            if (command != null && COMMAND_SET.contains(command))
                handleCommand(command, msg.trim().substring(msgParts[0].length()));
            else
                //if command is invalid
                this.self().tell(new FTPMessage(RepliesCodes.C_500.getCodeReplies()), self());

        } else if (message instanceof Tcp.ConnectionClosed) {
            log.info(String.format("%s connection closed!", remoteInetAddress));
            if (fileServer != null)
                context().stop(fileServer);
            context().stop(self());

        } else if (message instanceof FTPMessage) {
            final String ftpMessage = ((FTPMessage) message).getMessage();
            log.info(String.format("Send message: %s", ftpMessage));
            client.tell(TcpMessage.write(ByteString.fromString(ftpMessage)), self());
        } else {
            log.error("Fot smth. strange: " + message);
            unhandled(message);
        }
    }

    private String getPath(String p) {
        return (p.charAt(0) == '/' ? rootDirectory : currentDirectory) +
                ((p.charAt(0) == '\\' || p.charAt(0) == '/') ? "" : "\\") +
                p.replace("/", "\\");
    }

    private void handleCommand(Commands command, String args) {
        switch (command) {
            case SIZE:
                sizeCommand(args);
                break;
            case MDTM:
                mdtmCommand(args);
                break;
            case AUTH:
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_502.getCodeReplies()), self());
                break;
            case ABOR:
                aborCommand();
                break;
            case ACCT://no need for this command, because many clients don't have it
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_502.getCodeReplies()), self());
                break;
            case ALLO://allocate memory, obsolete command
                self().tell(new FTPMessage(RepliesCodes.C_202.getCodeReplies()), self());
                break;
            case APPE:
                appeCommand(args);
                break;
            case CDUP:
                cdupCommand();
                break;
            case CWD:
                cwdCommand(args);
                break;
            case DELE:
                deleCommand(args);
                break;
            case HELP:
                helpCommand(args);
                break;
            case NLST:
                nlstCommand(args);
                break;
            case LIST:
                listCommand(args);
                break;
            case XMKD:
            case MKD:
                mkdCommand(args);
                break;
            case MODE://obsolete, (S - stream(default), B- block, C- compressed)
                modeCommand(args);
                break;
            case NOOP://keep alive command
                self().tell(new FTPMessage(RepliesCodes.C_200.getCodeReplies()), self());
                break;
            case PASS:
                passCommand(args);
                break;
            case PASV:
                pasvCommand();
                break;
            case PORT:
                portCommand(args);
                break;
            case PWD:
            case XPWD:
                pwdCommand();
                break;
            case QUIT:
                quitCommand();
                break;
            case REIN:
                reinCommand();
                break;
            case REST://restart command, not implemented
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_502.getCodeReplies()), self());
                break;
            case RETR:
                retrCommand(args);
                break;
            case XRMD:
            case RMD:
                rmdCommand(args);
                break;
            case RNFR:
                rnfrCommand(args);
                break;
            case RNTO:
                rntoCommand(args);
                break;
            case SITE://for new ftp specifications
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_502.getCodeReplies()), self());
                break;
            case SMNT://change structure, maybe no need for this command
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_502.getCodeReplies()), self());
                break;
            case STAT://used by clients, to know LIST format and info about server
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_504.getCodeReplies()), self());
                break;
            case STOR:
                storCommand(args);
                break;
            case STOU:
                stouCommand();
                break;
            case STRU://maybe no need for this command
                log.warning("Unimplemented command " + command.name());
                self().tell(new FTPMessage(RepliesCodes.C_502.getCodeReplies()), self());
                break;
            case SYST:
                self().tell(new FTPMessage(RepliesCodes.C_215.getCodeReplies("Windows_NT")), self());
                break;
            case TYPE://by default use binary format, no need of ascii format
                self().tell(new FTPMessage(RepliesCodes.C_200.getCodeReplies()), self());
                break;
            case USER:
                userCommand(args);
                break;
            case OPTS:
                this.self().tell(new FTPMessage(RepliesCodes.C_200.getCodeReplies()), self());
                break;
            case FEAT:
                self().tell(new FTPMessage(RepliesCodes.C_211.getCodeReplies()), self());
                break;
            default:
                this.self().tell(new FTPMessage(RepliesCodes.C_500.getCodeReplies()), self());
        }
    }

    //================================================Commands==========================================================

    private void mdtmCommand(String filePath) {
        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);

        if (file.exists())
            self().tell(new FTPMessage(RepliesCodes.C_213.getCodeReplies(
                    new SimpleDateFormat("YYYYMMDDhhmmss").format(file.lastModified()))), self());
        else
            self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
    }

    private void helpCommand(String args) {
        if (args.equals("")) {
            self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.HELP.getHelp())), self());
        } else {
            try {
                switch (Commands.valueOf(args.trim().toUpperCase())) {
                    case SIZE:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.SIZE.getHelp())), self());
                        break;
                    case AUTH:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.AUTH.getHelp())), self());
                        break;
                    case ABOR:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.ABOR.getHelp())), self());
                        break;
                    case ACCT:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.ACCT.getHelp())), self());
                        break;
                    case ALLO:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.ALLO.getHelp())), self());
                        break;
                    case APPE:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.APPE.getHelp())), self());
                        break;
                    case CDUP:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.CDUP.getHelp())), self());
                        break;
                    case CWD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.CWD.getHelp())), self());
                        break;
                    case DELE:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.DELE.getHelp())), self());
                        break;
                    case HELP:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.HELP.getHelp())), self());
                        break;
                    case NLST:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.NLST.getHelp())), self());
                        break;
                    case LIST:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.LIST.getHelp())), self());
                        break;
                    case XMKD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.XMKD.getHelp())), self());
                        break;
                    case MKD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.MKD.getHelp())), self());
                        break;
                    case MODE:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.MODE.getHelp())), self());
                        break;
                    case NOOP:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.NOOP.getHelp())), self());
                        break;
                    case PASS:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.PASS.getHelp())), self());
                        break;
                    case PASV:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.PASV.getHelp())), self());
                        break;
                    case PORT:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.PORT.getHelp())), self());
                        break;
                    case PWD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.PWD.getHelp())), self());
                        break;
                    case XPWD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.XPWD.getHelp())), self());
                        break;
                    case QUIT:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.QUIT.getHelp())), self());
                        break;
                    case REIN:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.REIN.getHelp())), self());
                        break;
                    case REST:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.REST.getHelp())), self());
                        break;
                    case RETR:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.RETR.getHelp())), self());
                        break;
                    case XRMD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.XRMD.getHelp())), self());
                        break;
                    case RMD:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.RMD.getHelp())), self());
                        break;
                    case RNFR:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.RNFR.getHelp())), self());
                        break;
                    case RNTO:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.RNTO.getHelp())), self());
                        break;
                    case SITE:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.SITE.getHelp())), self());
                        break;
                    case SMNT:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.SMNT.getHelp())), self());
                        break;
                    case STAT:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.STAT.getHelp())), self());
                        break;
                    case STOR:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.STOR.getHelp())), self());
                        break;
                    case STOU:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.STOU.getHelp())), self());
                        break;
                    case STRU:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.STRU.getHelp())), self());
                        break;
                    case SYST:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.SYST.getHelp())), self());
                        break;
                    case TYPE:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.TYPE.getHelp())), self());
                        break;
                    case USER:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.USER.getHelp())), self());
                        break;
                    case OPTS:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.OPTS.getHelp())), self());
                        break;
                    case FEAT:
                        self().tell(new FTPMessage(RepliesCodes.C_214.getCodeReplies(Commands.FEAT.getHelp())), self());
                        break;
                    default:
                        self().tell(new FTPMessage(RepliesCodes.C_501.getCodeReplies()), self());
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                self().tell(new FTPMessage(RepliesCodes.C_501.getCodeReplies()), self());
            }
        }
    }

    private void modeCommand(String code) {
        String trimCode = code.trim();
        if (trimCode.toLowerCase().equals("S") || trimCode.toUpperCase().equals("S")) {
            self().tell(new FTPMessage(RepliesCodes.C_200.getCodeReplies()), self());//because obsolete command
        } else
            self().tell(new FTPMessage(RepliesCodes.C_504.getCodeReplies()), self());
    }

    private void nlstCommand(String args) {
        File file = new File(currentDirectory + "\\" + args.trim());
        if (file.exists() && file.isDirectory()) {
            if (fileServer != null) {
                context().system().scheduler().scheduleOnce(
                        Duration.create(100, TimeUnit.MILLISECONDS),
                        fileServer,
                        new FTPDataMessage(Commands.NLST, file.getPath()),
                        context().dispatcher(),
                        self());
                //fileServer.tell(new FTPDataMessage(Commands.NLST, file.getPath()), self());
            } else
                //if no port or pasv command has been done
                self().tell(new FTPMessage(RepliesCodes.C_425.getCodeReplies()), self());
        } else
            //if file isn't a dir or didn't exist
            self().tell(new FTPMessage(RepliesCodes.C_450.getCodeReplies()), self());
    }

    private void appeCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }

        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);
        try {
            if (fileServer != null) {
                //if file can be deleted
                if (!file.exists()) {
                    if (file.createNewFile())
                        fileServer.tell(new FTPDataMessage(Commands.STOR, file.getPath()), self());
                        //if no permission
                    else
                        self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
                }
                //if no file with this name
                else {
                    fileServer.tell(new FTPDataMessage(Commands.STOR, file.getPath()), self());
                }
            } else
                //if no port or pasv command has been done
                self().tell(new FTPMessage(RepliesCodes.C_425.getCodeReplies()), self());
        } catch (IOException e) {
            e.printStackTrace();
            self().tell(new FTPMessage(RepliesCodes.C_552.getCodeReplies()), self());
        }

    }

    private void stouCommand() {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }

        File file = new File(currentDirectory + "\\" + NEW_FILE);
        int fileIndex = 2;

        while (file.exists())
            file = new File(currentDirectory + "\\" + NEW_FILE + "(" + fileIndex++ + ")");

        try {
            if (fileServer != null) {
                //if file can be deleted
                if (file.createNewFile())
                    fileServer.tell(new FTPDataMessage(Commands.STOR, file.getPath()), self());
                    //if no permission
                else
                    self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            } else
                //if no port or pasv command has been done
                self().tell(new FTPMessage(RepliesCodes.C_425.getCodeReplies()), self());
        } catch (Exception e) {
            e.printStackTrace();
            self().tell(new FTPMessage(RepliesCodes.C_552.getCodeReplies()), self());
        }
    }

    private void storCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }

        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);

        try {
            if (fileServer != null) {
                //if file can be deleted
                if (file.exists() && file.renameTo(file)) {
                    if (file.delete()) {
                        if (file.createNewFile()) {
                            self().tell(new FTPMessage(RepliesCodes.C_150.getCodeReplies()), self());
                            fileServer.tell(new FTPDataMessage(Commands.STOR, file.getPath()), self());
                        }
                        //if no permission
                        else
                            self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
                    }
                    //if no permission
                    else {
                        self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
                    }
                }
                //if no file with this name
                else {
                    if (file.createNewFile())
                        fileServer.tell(new FTPDataMessage(Commands.STOR, file.getPath()), self());
                        //if no permission
                    else
                        self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
                }
            } else
                //if no port or pasv command has been done
                self().tell(new FTPMessage(RepliesCodes.C_425.getCodeReplies()), self());
        } catch (IOException e) {
            e.printStackTrace();
            self().tell(new FTPMessage(RepliesCodes.C_552.getCodeReplies()), self());
        }
    }

    private void rntoCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }
        if (rnfrFile == null || !rnfrFile.exists()) {
            self().tell(new FTPMessage(RepliesCodes.C_503.getCodeReplies()), self());
            return;
        }

        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);

        try {
            if (rnfrFile.renameTo(file)) {
                rnfrFile = null;
                self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
            } else self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
        } catch (Exception e) {
            e.printStackTrace();
            self().tell(new FTPMessage(RepliesCodes.C_553.getCodeReplies()), self());
        }
    }

    private void rnfrCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }
        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);
        if (file.exists()) {
            rnfrFile = file;
            self().tell(new FTPMessage(RepliesCodes.C_350.getCodeReplies()), self());
        } else
            self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
    }

    private void rmdCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }
        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);
        if (file.exists() && file.isDirectory()) {
            if (file.delete()) {
                self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
            } else self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
        } else self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
    }

    private void mkdCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }
        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);

        if (file.exists()) {
            self().tell(new FTPMessage(RepliesCodes.C_501.getCodeReplies()), self());
            return;
        }

        if (file.mkdir()) {
            String newPath;
            if (p.charAt(0) == '/') {
                currentDirectory = path;// FIXME: 12-Dec-16 need to change curDir depending on type of path (absolute, relative)
                newPath = "\"" + currentDirectory.substring(rootDirectory.length()).replace("\\", "/") + "\"";
            } else {
                newPath = currentDirectory;
            }
            self().tell(new FTPMessage(RepliesCodes.C_257.getCodeReplies(newPath)), self());
        } else {
            self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
        }
    }

    private void deleCommand(String filePath) {
        if (userName.equals(ANONYMOUS)) {
            self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
            return;
        }
        String p = filePath.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            if (file.delete())
                self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
            else self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
        } else
            //if file isn't a dir or didn't exist
            self().tell(new FTPMessage(RepliesCodes.C_450.getCodeReplies()), self());
    }

    private void aborCommand() {
        JavaConversions.asJavaIterable(context().children()).forEach(child -> {
            child.tell(TcpMessage.abort(), self());
            child.tell(PoisonPill.getInstance(), ActorRef.noSender());
        });
    }

    private void sizeCommand(String path) {
        File file = new File(rootDirectory + path.trim().replace("/", "\\"));
        if (file.exists()) {
            self().tell(new FTPMessage(RepliesCodes.C_213.getCodeReplies(file.length())), self());
        } else {
            self().tell(new FTPMessage(RepliesCodes.C_450.getCodeReplies()), self());
        }
    }

    private void listCommand(String args) {
        File file = new File(currentDirectory + "\\" + args.trim());
        if (file.exists() && file.isDirectory()) {
            if (fileServer != null) {
                context().system().scheduler().scheduleOnce(
                        Duration.create(100, TimeUnit.MILLISECONDS),
                        fileServer,
                        new FTPDataMessage(Commands.LIST, file.getPath()),
                        context().dispatcher(),
                        self());
                //fileServer.tell(new FTPDataMessage(Commands.LIST, file.getPath()), self());
            } else
                //if no port or pasv command has been done
                self().tell(new FTPMessage(RepliesCodes.C_425.getCodeReplies()), self());
        } else
            //if file isn't a dir or didn't exist
            self().tell(new FTPMessage(RepliesCodes.C_450.getCodeReplies()), self());
    }

    private void retrCommand(String args) {
        String p = args.trim();

        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        String path = getPath(p);
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            if (fileServer != null) {
                fileServer.tell(new FTPDataMessage(Commands.RETR, file.getPath()), self());
            } else
                //if no port or pasv command has been done
                self().tell(new FTPMessage(RepliesCodes.C_425.getCodeReplies()), self());
        } else
            //if no such file
            self().tell(new FTPMessage(RepliesCodes.C_450.getCodeReplies()), self());
    }

    private void pasvCommand() {
        fileServerPort = ThreadLocalRandom.current().nextInt(MIN_PORT_NUM, MAX_PORT_NUM + 1);
        boolean isNewActive = false;
/*
        if (isActive || fileServer == null) {
*/
/*        if (fileServer != null)
        {
            fileServer.tell(PoisonPill.getInstance(), ActorRef.noSender());
        }*/

        fileServer = context().actorOf(FileServer.props(
                null,
                null,
                fileServerPort,
                serverInetAddress,
                isNewActive,
                self()));
/*
        } else
            self().tell(new FTPMessage(RepliesCodes.C_227.getCodeReplies(getFileServerAddresAndPort())), self());
*/


        isActive = isNewActive;
//        fileServer = new FileServer(serverInetAddress, fileServerPort, this.self());
//        context().actorOf(FileServer.props(fileServer));
    }

    private void userCommand(String userName) {
        //if user - anonymous
        if (userName == null || userName.trim().equals("")) {
            this.self().tell(new FTPMessage(RepliesCodes.C_501.getCodeReplies(userName)), self());
            userName = ANONYMOUS;
        } else {
            this.userName = userName.trim();
            //ask for password
            this.self().tell(new FTPMessage(RepliesCodes.C_331.getCodeReplies(userName)), self());
        }
    }

    private void passCommand(String pass) {
        if (pass == null)
            this.self().tell(new FTPMessage(RepliesCodes.C_503.getCodeReplies()), self());
        else if (userName.equals(ANONYMOUS))
            this.self().tell(new FTPMessage(RepliesCodes.C_230.getCodeReplies(userName)), self());
        else {
            this.pass = pass.trim();
            if (SettingsParser.getInstance().isValidUser(userName, this.pass))
                this.self().tell(new FTPMessage(RepliesCodes.C_230.getCodeReplies(userName)), self());
            else//if invalid username or pass
            {
                self().tell(new FTPMessage(RepliesCodes.C_530.getCodeReplies()), self());
                self().tell(new FTPMessage(RepliesCodes.C_421.getCodeReplies()), self());

                //send message after delay
                context().system().scheduler().scheduleOnce(Duration.create(100, TimeUnit.MILLISECONDS),
                        client, TcpMessage.close(), context().dispatcher(), ActorRef.noSender());
            }
        }
    }

    private void reinCommand() {
        this.userName = ANONYMOUS;
        this.pass = "";
        this.self().tell(new FTPMessage(RepliesCodes.C_220.getCodeReplies()), self());
    }

    private void portCommand(String args) {
        String[] split = args.trim().split(",");

        try {
            clientFileServerAddr = InetAddress.getByAddress(
                    new byte[]{
                            Integer.valueOf(split[0]).byteValue(),
                            Integer.valueOf(split[1]).byteValue(),
                            Integer.valueOf(split[2]).byteValue(),
                            Integer.valueOf(split[3]).byteValue()});

            int factor = Integer.valueOf(split[4]);
            int add = Integer.valueOf(split[5]);

            clientFileServerPort = factor * PORT_FACTOR + add;

            boolean isNewActive = true;

/*            if (fileServer != null)
                fileServer.tell(PoisonPill.getInstance(), ActorRef.noSender());*/

            fileServer = context().actorOf(FileServer.props(clientFileServerPort,
                    clientFileServerAddr,
                    null,
                    null,
                    isNewActive,
                    this.self()));

            isActive = isNewActive;
        } catch (Exception e) {
            e.printStackTrace();
            this.self().tell(new FTPMessage(RepliesCodes.C_501.getCodeReplies()), self());
        }
    }

    private void cdupCommand() {
        if (rootDirectory.equals(currentDirectory))
            this.self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
        else {
            int index = currentDirectory.lastIndexOf("\\");
            currentDirectory = currentDirectory.substring(0, index);
            this.self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
        }
    }

    private void pwdCommand() {
        if (currentDirectory.equals(rootDirectory))
            this.self().tell(new FTPMessage(RepliesCodes.C_257.getCodeReplies("\"/\"")), self());
        else {
            String path = currentDirectory.substring(rootDirectory.length()).replace("\\", "/");
            this.self().tell(new FTPMessage(RepliesCodes.C_257.getCodeReplies("\"" + path + "\"")), self());
        }
    }

    private void cwdCommand(String args) {
        String p = args.trim();
        if (p.length() == 0) {
            this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
            return;
        }

        if (p.equals("/")) {
            currentDirectory = rootDirectory;
        } else {
            String path = getPath(p);

            File file = new File(path);

            String canoncicalPath = null;
            try {
                canoncicalPath = file.getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (canoncicalPath != null && canoncicalPath.length() < rootDirectory.length()) {
                self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
                return;
            }

            if (!(file.exists() && file.isDirectory()) || file.getPath().equals(currentDirectory)) {
                this.self().tell(new FTPMessage(RepliesCodes.C_550.getCodeReplies()), self());
                return;
            }

            currentDirectory = path;
        }
        this.self().tell(new FTPMessage(RepliesCodes.C_250.getCodeReplies()), self());
    }

    private void quitCommand() {
        this.self().tell(new FTPMessage(RepliesCodes.C_221.getCodeReplies()), self());
        this.self().tell(TcpMessage.close(), self());
    }

}
