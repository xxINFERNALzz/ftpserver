package sample;

import Messages.Commands;
import Messages.FTPDataMessage;
import Messages.FTPMessage;
import Messages.RepliesCodes;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.io.Tcp;
import akka.io.TcpMessage;
import akka.japi.Creator;
import akka.util.ByteString;
import com.sun.istack.internal.NotNull;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by xxinf on 02-Nov-16.
 */
public class FileServer extends UntypedActor {
    private int activeClientPort;
    private InetAddress activeClientInetAddr;
    private final int fileServerPort;
    private InetAddress fileServerInetAddr;

    private boolean isActive;
    private AtomicBoolean isConnected = new AtomicBoolean(false);

    private final ActorRef msgHandler;

    private ActorRef clientData = null;
    private final ActorRef tcpManager = Tcp.get(context().system()).manager();
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    private File saveFile;

    private final static int DEFAULT_ACTIVE_SERVER_PORT = 20;

    public static Props props(final Integer clientPort,
                              final InetAddress clientInetAddr,
                              final Integer fileServerPort,
                              final InetAddress fileServerInetAddr,
                              @NotNull final boolean isActive,
                              @NotNull final ActorRef msgHanler) {

        return Props.create(new Creator<FileServer>() {
            public FileServer create() throws Exception {
                return isActive ?
                        new FileServer(clientPort, clientInetAddr, msgHanler) :
                        new FileServer(fileServerInetAddr, fileServerPort, msgHanler);
            }
        });
    }

    /**
     * Active file server
     *
     * @param clientPort     - represent client port
     * @param clientInetAddr - represent client IP
     */
    public FileServer(@NotNull int clientPort,
                      @NotNull InetAddress clientInetAddr,
                      @NotNull ActorRef msgHandler) {
        this.activeClientPort = clientPort;
        this.activeClientInetAddr = clientInetAddr;
        this.fileServerPort = DEFAULT_ACTIVE_SERVER_PORT;
        isActive = true;
        this.msgHandler = msgHandler;
        tcpManager.tell(TcpMessage.connect(new InetSocketAddress(clientInetAddr, clientPort)), getSelf());
        log.info(String.format("FileServer connecting to client port ", clientPort));
    }


    /**
     * Passive file server
     *
     * @param serverInetAddr - ip of fileServer
     * @param fileServerPort - port number of fileServer
     */
    public FileServer(InetAddress serverInetAddr, int fileServerPort, ActorRef msgHandler) {
        this.fileServerInetAddr = serverInetAddr;
        this.fileServerPort = fileServerPort;
        this.msgHandler = msgHandler;
        isActive = false;
        tcpManager.tell(
                TcpMessage.bind(self(), new InetSocketAddress(fileServerInetAddr, fileServerPort), 100),
                self());
        log.info(String.format("FileServer starting on port ", fileServerPort));
    }

    public void onReceive(Object message) throws Exception {
        if (message instanceof Tcp.Bound) {

            log.info(String.format("FileServer bounded to port %d", fileServerPort));

            Thread.currentThread().sleep(100);
            if (!isActive) {
                msgHandler.tell(new FTPMessage(RepliesCodes.C_227.getCodeReplies(getFileServerAddresAndPort())), self());
//                isConnected.set(true);
            }

        } else if (message instanceof Tcp.CommandFailed) {

            log.error(String.format("FileServer command Failed -> cannot connect to port %d", fileServerPort));
            context().stop(self());

        } else if (message instanceof Tcp.Connected) {

            final Tcp.Connected connected = ((Tcp.Connected) message);
            log.info(String.format("Client has been connected to FileServer", connected.remoteAddress()));
            tcpManager.tell(connected, self());

            ActorRef sender = getSender();
            sender.tell(TcpMessage.register(self()), self());
            clientData = sender;

            if (!isActive) {
                //msgHandler.tell(new FTPMessage(RepliesCodes.C_227.getCodeReplies(getFileServerAddresAndPort())), self());
            } else
                msgHandler.tell(new FTPMessage(RepliesCodes.C_200.getCodeReplies()), self());
            isConnected.set(true);

        } else if (message instanceof Tcp.Received) {
            if (saveFile != null) {
                try (FileOutputStream outputStream = new FileOutputStream(saveFile, true)) {
                    outputStream.write(((Tcp.Received) message).data().toArray());
                    outputStream.flush();
                }
                //self().tell(new Ack(RepliesCodes.C_226.getCodeReplies()), self());//// FIXME: 12-Dec-16 add close connection depending on mode
            }
        } else if (message instanceof Tcp.ConnectionClosed) {

            isConnected.set(false);
            log.debug("FileServer connection closed!");

        } else if (message instanceof Tcp.Event) {

            ((Ack) message).sendResult();
            clientData.tell(TcpMessage.close(), self());
            if (!isActive)
                self().tell(PoisonPill.getInstance(), ActorRef.noSender());

        } else if (message instanceof FTPDataMessage) {

            handleCommand(((FTPDataMessage) message));
        }
    }

    /**
     * @return <p>6 int values in format:
     * <br>first 4 int - numbers of fileServer IP address
     * <br>last 2 int - is fileServer port, represented as 5-th * 256 + 6-th int</p>
     */
    private Object[] getFileServerAddresAndPort() {
        List<Integer> result = new ArrayList<>();

        byte[] fileServerAddr = fileServerInetAddr.getAddress();
        IntStream.range(0, fileServerAddr.length)
                .forEach(i -> result.add((int) fileServerAddr[i] < 0 ? (fileServerAddr[i] + 256) : fileServerAddr[i]));

        int n6 = fileServerPort % MessageHandler.PORT_FACTOR;
        int n5 = (fileServerPort - n6) / MessageHandler.PORT_FACTOR;

        result.add(n5);
        result.add(n6);

        return result.toArray();
    }

    private void handleCommand(FTPDataMessage dataMessage) {
        switch (dataMessage.getCommand()) {
            case RETR:
                reconnect();
                retrCommand(new File(dataMessage.getData()));
                //waitAndDisconnect();
                break;
            case LIST:
                reconnect();
                listCommand(new File(dataMessage.getData()));
                //waitAndDisconnect();
                break;
            case NLST:
                reconnect();
                nlstCommand(new File(dataMessage.getData()));
                break;
            case STOR:
                reconnect();//// FIXME: 12-Dec-16 maybe need to change order fo this commands
                storCommand(new File(dataMessage.getData()));
                break;
            default:
                log.error("Unknown command received, FileServer -> handle! " + dataMessage.getCommand());
        }
    }

    private void reconnect() {
        //if worked
        if (!isConnected.get() && clientData != null) {
            msgHandler.tell(new FTPMessage(RepliesCodes.C_150.getCodeReplies()), self());
            if (isActive) {
                tcpManager.tell(TcpMessage.connect(new InetSocketAddress(activeClientInetAddr, activeClientPort)), getSelf());
                log.info(String.format("FileServer connecting to client port ", activeClientPort));
            } else {
                tcpManager.tell(
                        TcpMessage.bind(self(), new InetSocketAddress(fileServerInetAddr, fileServerPort), 100),
                        self());
                log.info(String.format("FileServer starting on port ", fileServerPort));
            }
        } else {
            msgHandler.tell(new FTPMessage(RepliesCodes.C_125.getCodeReplies()), self());
        }
    }

    private void waitAndDisconnect() {

        if (isConnected.get()) {
            clientData.tell(TcpMessage.close(), self());
            isConnected.set(false);
        }
    }

    //================================================Commands==========================================================
    private class Ack implements Tcp.Event {
        String result;

        public Ack(String result) {
            this.result = result;
        }

        public void sendResult() {
            msgHandler.tell(new FTPMessage(result), self());
        }
    }

    private void retrCommand(File file) {
        if (clientData != null)
            if (file.length() != 0)
                clientData.tell(TcpMessage.writeFile(file.getPath(), 0, file.length(),
                        new Ack(RepliesCodes.C_250.getCodeReplies())), self());
            else
                self().tell(new Ack(RepliesCodes.C_250.getCodeReplies()), self());
        else//send message with delay
            context().system().scheduler().scheduleOnce(Duration.create(50, TimeUnit.MILLISECONDS),
                    self(), new FTPDataMessage(Commands.RETR, file.getPath()), context().dispatcher(), self());
    }

    private void listCommand(File file) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            try (Stream<Path> stream = Files.list(file.toPath())) {
                stream.forEach(path -> {
                    File f = path.toFile();
                    /*
                    Answer format:
                            "03-26-07  05:23AM       <DIR>          html\n" +
                            "03-26-07  04:27AM       <DIR>          ibm-laptop\n" +
                            "03-26-07  03:16AM       <DIR>          images\n" +
                            "03-27-07  11:00PM                 6397 index.html\n" +
                            "03-26-07  03:45AM                10186 index1.html"
                    */
                    stringBuilder.append(new SimpleDateFormat("MM-dd-yy  HH:mma").format(file.lastModified()));
                    stringBuilder.append(" " + (f.isFile() ?
                            (getSpaces(f.length()) + f.length() + " ")
                            : ("      <DIR>" + "          ")));
                    stringBuilder.append(f.getName() + "\n");
                });
            }
            ByteString msg = ByteString.fromArray(stringBuilder.toString().getBytes(/*"UTF-8"*/"US-ASCII"));

            if (clientData != null)
                clientData.tell(TcpMessage.write(msg, new Ack(RepliesCodes.C_226.getCodeReplies())), self());
        } catch (IOException e) {
            e.printStackTrace();
            self().tell(new Ack(RepliesCodes.C_451.getCodeReplies()), self());
        }
    }

    private String getSpaces(long number) {
        final StringBuilder stringBuilder = new StringBuilder();
        final int digits = 1 + (int) Math.floor(Math.log10(number));
        for (int i = 20 - digits; i > 0; --i)
            stringBuilder.append(" ");
        return stringBuilder.toString();
    }

    private void storCommand(File file) {
        saveFile = file;
    }

    private void nlstCommand(File file) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            try (Stream<Path> stream = Files.list(file.toPath())) {
                stream.forEach(path -> {
                    File f = path.toFile();
                    stringBuilder.append(f.getName() + "\n");
                });
            }
            ByteString msg = ByteString.fromArray(stringBuilder.toString().getBytes("US-ASCII"));

            if (clientData != null)
                clientData.tell(TcpMessage.write(msg, new Ack(RepliesCodes.C_226.getCodeReplies())), self());
        } catch (IOException e) {
            e.printStackTrace();
            self().tell(new Ack(RepliesCodes.C_451.getCodeReplies()), self());
        }

    }

}
