package sample;

import Util.SettingsParser;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.io.Tcp;
import akka.io.TcpMessage;
import akka.japi.Creator;

import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 * Created by xxinf on 31-Oct-16.
 */
public class FTPServer extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private final ActorRef tcpManager = Tcp.get(context().system()).manager();

    private final int port;
    private final String interfaceAddress;

    private final static int DEFAULT_PORT = 21;
    private final static String DEFAULT_INTERFACE_ADDRESS = "localhost";

    public static Props props() {
        return Props.create(new Creator<FTPServer>() {
            @Override
            public FTPServer create() throws Exception {
                return new FTPServer(SettingsParser.getInstance().getServerPort(),
                        SettingsParser.getInstance().getInterfaceAddress().getHostAddress());
            }
        });
    }

    private FTPServer(Integer port, String interfaceAddress) {
        if (port != null)
            this.port = port;
        else
            this.port = DEFAULT_PORT;

        if (interfaceAddress != null)
            this.interfaceAddress = interfaceAddress;
        else
            this.interfaceAddress = DEFAULT_INTERFACE_ADDRESS;
    }

    @Override
    public void preStart() throws Exception {
        final InetSocketAddress inetAddress = new InetSocketAddress(interfaceAddress, port);
        tcpManager.tell(TcpMessage.bind(self(), inetAddress, 100), self());
        log.info(String.format("Server starting on port %d...", port));
    }

    public void onReceive(Object message) throws Exception {
        if (message instanceof Tcp.Bound) {
            log.info(String.format("Server bounded to port %d", port));
        } else if (message instanceof Tcp.CommandFailed) {
            log.info(String.format("Server command Failed -> cannot connect to port %d", port));
            context().stop(self());
        } else if (message instanceof Tcp.Connected) {
            final Tcp.Connected connected = ((Tcp.Connected) message);
            log.info(String.format("New client has been connected ", connected.remoteAddress()));

            tcpManager.tell(connected, self());

            final ActorRef msgHandler = context().actorOf(
                    MessageHandler.props(
                            connected.remoteAddress().getAddress(),
                            InetAddress.getByName(interfaceAddress),
                            getSender()));

            getSender().tell(TcpMessage.register(msgHandler), self());
        }
    }
}
