package Util;


import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xxinf on 31-Oct-16.
 */
public class SettingsParser {
    private static SettingsParser instance;

    private InetAddress serverAddres = InetAddress.getByName("127.0.0.1");
    private String workingDirectory = new File("").getAbsolutePath();

    private static final ConcurrentHashMap<String, String> USERS = new ConcurrentHashMap<>(Collections.singletonMap("admin", "admin"));

    private static final String SHARE_FOLDER = "ShareFolder";
    private static final String IP = "IP";
    private static final String LOGIN = "Login";
    private static final String PASS = "Pass";
    private static final String SETTINGS_FILE = "serverSettings.txt";

    private SettingsParser() throws UnknownHostException {
        instance = this;
    }

    public static SettingsParser getInstance() {
        if (instance == null)
            try {
                return new SettingsParser();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return null;
            }
        else return instance;
    }

    public InetAddress getInterfaceAddress() {
        return serverAddres;
    }

    public String getWorkingDirectory() {
        return workingDirectory;
    }

    public boolean isValidUser(String user,String pass) {
        String userPass = USERS.get(user);

        return userPass != null && userPass.equals(pass);
    }

    public int getServerPort() {
        return 21;
    }

    public boolean createSettings() {
        File settingsFile = new File(SETTINGS_FILE);

        if (settingsFile.exists()) {
            if (!parseSettings(settingsFile))
                return false;
        } else {
            try {
                if (settingsFile.createNewFile() && initDefaultSettings(settingsFile))
                    return true;
                else
                    return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private boolean initDefaultSettings(File settingsFile) {
        try {
            try (PrintWriter pw = new PrintWriter(settingsFile)) {
                pw.println(SHARE_FOLDER + "=" + workingDirectory);
                pw.println(IP + "=" + serverAddres.getHostAddress());
                pw.println("Users:");
                pw.println(LOGIN + "=" + "admin");
                pw.println(PASS + "=" + "admin");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean parseSettings(File settingsFile) {
        try {
            try (Scanner scanner = new Scanner(settingsFile)) {

                Pattern pattern = Pattern.compile("(ShareFolder|IP|Login|Pass)=(.*)$");
                String user = null;

                boolean isAdminPresented = false;

                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    if (line.trim().equals("Users:"))
                        continue;
                    else {
                        Matcher matcher = pattern.matcher(line.trim());
                        if (matcher.matches()) {
                            switch (matcher.group(1)) {
                                case SHARE_FOLDER:
                                    this.workingDirectory = matcher.group(2);
                                    break;
                                case IP:
                                    this.serverAddres = InetAddress.getByName(matcher.group(2));
                                    break;
                                case LOGIN:
                                    user = matcher.group(2);
                                    break;
                                case PASS:
                                    matcher.group(2);
                                    if (user != null) {
                                        USERS.put(user, matcher.group(2));
                                        if (user.equals("admin") && matcher.group(2).equals("admin"))
                                            isAdminPresented = true;
                                    } else throw new Exception("Invalid settings file!");
                                    break;
                            }
                        }
                    }
                }
                if (!isAdminPresented)
                    USERS.remove("admin", "admin");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
