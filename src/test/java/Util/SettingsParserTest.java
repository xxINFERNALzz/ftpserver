package Util;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by xxinf on 31-Oct-16.
 */
public class SettingsParserTest {

    private SettingsParser settingsParser;

    @Before
    public void init() {
        this.settingsParser = SettingsParser.getInstance();
    }

    @Test
    public void test_get_valid_port() {
        Assert.assertEquals(21, settingsParser.getServerPort());
    }

    @Test
    public void test_singleton_instanse() throws ExecutionException, InterruptedException {
        final int THREADS = 1000;
        ExecutorService executorService = Executors.newFixedThreadPool(THREADS);
        for(int i = 0; i < THREADS; ++i) {
            Assert.assertSame(
                    executorService.submit((Callable<SettingsParser>) () -> SettingsParser.getInstance()).get(),
                    settingsParser);
        }
    }


}
